import ticketsReducers from './ticketsReducer'
import { combineReducers } from 'redux'

const allReducers = combineReducers({
    ticketList: ticketsReducers
});

export default allReducers;