const initialState = {

}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'GET_TICKETS_SUCCESS':            
            return {
                ...state,
                tickets: action.payload
            };
        case 'REQUEST_FAILED': {
            console.log('REQUEST_FAILED');
            console.debug(action);
            break;
        }
        default:
    }
    return state;
};
