import React from 'react';
import Logout from './components/Logout'
import Login from './components/Login';

const Navbar = (props) => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light mb-3" >
            <div className="collapse navbar-collapse">
                <ul className="navbar-nav mr-auto mt-2 mt-lg-0">

                    <li className="nav-item">
                    <Login/>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/tickets">Tickets</a>
                    </li>
                    <li className="nav-item">  <Logout /></li>

                </ul>
                {/* <form method="post" action="/logout" class="form-inline my-2 my-lg-0" role="search">
                    <input name="logout" type="submit" class="btn btn-dark my-2 my-sm-0 ml-1 mx-1" value="Sign Out" />
                </form> */}
            </div>
        </nav>

    );
};
export default Navbar