import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { providerConfig } from '../constants/config'
import { OAuthProvider } from './OAuthProvider'


class Login extends Component {

    constructor(props) {
        super(props);
        this.state = { token: window.localStorage.getItem('OAuthProvider_token') };
        this.onOAuthProviderLogin = this.onOAuthProviderLogin.bind(this);
        this.onOAuthProviderLoginFailure = this.onOAuthProviderLoginFailure.bind(this);

    };
    onOAuthProviderLogin(data) {
        console.log("All ok")
        let token = JSON.stringify(data.code) || JSON.stringify(data);
        window.localStorage.setItem('OAuthProvider_token',token );
        this.setState({token:token});
        window.location = "/home"
    }

    onOAuthProviderLoginFailure(err) {
        console.log("something wrong")
        console.error(err);
    }

    onHandleChange(event) {
        let isValid = false;
        const val = event.target.value;
        switch (event.target.name) {
            case "username": {
                let regex = RegExp("^[^\.@]" + ".+" + "@" + ".+" + "\." + "[^\.@]+$");
                isValid = regex.test(val);
                this.setState(prevState => ({
                    isValid: {
                        ...prevState.isValid,
                        "username": isValid
                    }
                }))
            } break;
            case "password": {
                let regex = RegExp("^" +
                    "(?=.*[0-9])" +
                    "(?=.*[a-z])" +
                    "(?=.*[A-Z])" +
                    "(?=.*[\\][~.\"();:!@#$%^&*_+-/`{}])" +
                    ".{6,20}" +
                    "$");
                isValid = regex.test(val);
                this.setState(prevState => ({
                    isValid: {
                        ...prevState.isValid,
                        "password": isValid
                    }
                }));
            } break;
        }
        this.setState({ [event.target.name]: event.target.value });
    };
    componentDidMount() {
        this.setState({ token: window.localStorage.getItem('OAuthProvider_token') });
    }
    render() {
        
        return (
            this.state.token == null ?
                <OAuthProvider
                    config={providerConfig}
                    successCallback={this.onOAuthProviderLogin}
                    errorCallback={this.onOAuthProviderLoginFailure}
                    textDisplay='Login'
                    className="btn btn-success"
                />
                : ""
        )
    };
}



export default Login;