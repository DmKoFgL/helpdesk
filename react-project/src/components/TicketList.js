import React, { Component } from 'react';

export default class TicketList extends Component {
    render() {
        return (

            <div className="container w-75">
                <table className="table table-hover table-bordered">
                    <thead>
                        <tr className="table-active">
                            <th><span className="btn" onClick={() => this.props.onSort("id")}>ID</span></th>
                            <th><span className="btn" onClick={() => this.props.onSort("Name")}>Name</span></th>
                            <th><span className="btn" onClick={() => this.props.onSort("DesiredDate")}>Desired Date</span>
                            </th>
                            <th><span className="btn" onClick={() => this.props.onSort("Urgency")}>Urgency</span></th>
                            <th><span className="btn" onClick={() => this.props.onSort("Status")}>Status</span></th>
                            <th><span>Action</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.tickets && this.props.tickets.map(ticket => (
                                <tr>
                                    <td>{ticket.id}</td>
                                    <td><a href={"/tickets/" + ticket.id}>{ticket.name}</a></td>
                                    <td>{ticket.desiredResolutionDate && new Date(ticket.desiredResolutionDate).toLocaleDateString()}</td>
                                    <td>{ticket.urgency}</td>
                                    <td>{ticket.state}</td>
                                    <td>Action</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}