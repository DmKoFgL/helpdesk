import React, { Component } from 'react';


class Logout extends Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
        this.state={ token: window.localStorage.getItem('OAuthProvider_token') };
    }

    componentDidMount() {
       this.setState({ token: window.localStorage.getItem('OAuthProvider_token') });
    }
    logout() {
        window.localStorage.removeItem('OAuthProvider_token');
        window.location ="/home"
    }
    render() {
        return (
            <div>
                {this.state.token==null ? "":
                    <button onClick={this.logout} className="btn btn-dark">Logout</button> 
                }
            </div>
        )
    };

}
function mapStateToProps(state) {
    return {
        ...state,
        auth: state.authBackend
    };
};

export default Logout;