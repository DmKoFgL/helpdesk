import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getTickets } from '../actions/ticketsAction';
import TicketsForm from '../components/ticketsForm';
import TicketList from '../components/TicketList';

class Tickets extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            tickets: [],
            sorter: { field: "DEFAULT", isAsc: true },
            filter: { field: "", value: "" },
            formState: "ALL_TICKETS",
        });
        this.onGetMyTickets = this.onGetMyTickets.bind(this);
        this.onGetAllTickets = this.onGetAllTickets.bind(this);

        this.onSort = this.onSort.bind(this);
        this.onFilterAndSort = this.onFilterAndSort.bind(this);
    }

    componentDidMount() {
        this.onGetAllTickets();
    };
    componentWillReceiveProps(nextProps) {
        this.setState({ tickets: nextProps.tickets })
    }

    onGetMyTickets() {
        this.onFilterAndSort();
    }

    onGetAllTickets() {
        this.onFilterAndSort();
    }

    onSort(field) {
        let newSorter = this.state.sorter;

        if (newSorter.field === field) {
            newSorter.isAsc = !newSorter.isAsc;
        } else {
            newSorter.field = field;
            newSorter.isAsc = true;
        }
        this.setState({ sorter: newSorter });
        this.onFilterAndSort();
    }

    onFilterAndSort() {
        let ticketList;
        let mode = this.state.formState === "ALL_TICKETS" ? "ALL" : "MY";
        this.props.getTickets(mode, this.state.sorter, this.state.filter);
        console.log("state:")
        console.debug(this.state)
    }

    render() {
        return (
            <div>
                <TicketsForm onFilter={this.onFilter} onGetAllTickets={this.onGetAllTickets}
                    onGetMyTickets={this.onGetMyTickets}
                    btnClassAllTickets={this.state.formState === "ALL_TICKETS" ? "btn btn-primary mr-2 w-50" : "btn btn-outline-primary mr-2 w-50"}
                    btnClassMyTickets={this.state.formState === "MY_TICKETS" ? "btn btn-primary mr-2 w-50" : "btn btn-outline-primary mr-2 w-50"} />
                <TicketList tickets={this.state.tickets} onSort={this.onSort} />
            </div>
        );
    }

}
function mapStateToProps(props) {
    return {
        tickets: props.ticketList.tickets
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getTickets
    }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Tickets);