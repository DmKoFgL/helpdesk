import axios from 'axios'

export const getTicketList = () => dispatch => {

    axios('http://localhost:8080/rest/tickets', {
        method: 'GET',
       
    })
        .then(res => {
            dispatch({
                type: 'GET_TICKETS_SUCCESS',
                payload: res.data
            });
        }).catch(res => {
           
    dispatch({
        type: 'REQUEST_FAILED',
        payload: res
    });
})
};