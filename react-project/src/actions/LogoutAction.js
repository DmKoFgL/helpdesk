import axios from 'axios'

export const logout = (user) => dispatch => {
    axios.get("http://localhost:8080/logout")
        .then(res => {                     
            dispatch({
                type: "SUCCESS_LOGOUT"
            });
        }).catch(res => {                     
            dispatch({
                type: 'LOGOUTT_FAILED'
            });
        })
};