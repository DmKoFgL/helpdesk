import axios from 'axios'
export const getTickets = (mode = "ALL", sorter, filter) => dispatch => {
    let token = JSON.parse(window.localStorage.getItem("OAuthProvider_token"));
    let params = { access_token: token.access_token };

    if (filter != undefined && filter.field !== "" && filter !== {}) {
        params.field = filter.field;
        params.value = filter.value;
    }
    if (sorter != undefined) {
        params.sorterField = sorter.field;
        params.asc = sorter.isAsc;
    }
    console.debug(params);
    axios.get('http://localhost:8080/rest/tickets/' + mode, {
        params: params
    }).then(res => {
        dispatch({
            type: 'GET_TICKETS_SUCCESS',
            payload: res.data
        });
    }).catch(err => {
        dispatch({
            type: 'REQUEST_FAILED'
        });
    })
};
