import axios from 'axios'
 axios.defaults.withCredentials = true;
export const authenticated = (user) => dispatch => {
  
    axios('http://localhost:8080/rest/hi', {
        method: 'POST',
        auth: user
    })
        .then(res => {                     
            dispatch({
               type: 'LOGIN_SUCCESS',
                payload: res
            });
        }).catch(res => {                     
            dispatch({
                type: 'LOGIN_REQUEST_FAILED',
                payload: res
            });
        })
};