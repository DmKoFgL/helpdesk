import React, { Component } from 'react';
import Navbar from './navbar'
import './App.css';
import { Provider } from 'react-redux';
import store from './store';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Switch from '../node_modules/react-router-dom/Switch';
import Login from './components/Login'
import './bootstrap.min.css'
import Tickets from './containers/tickets';
import PrivateRoute from './components/PrivateRoute';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <Provider store={store}>
          <Router>
            <Switch>
              <Route exact path='/login' component={Login} />
              <Route exact path='/logout' component={Login} />
              <PrivateRoute exact path='/tickets' component={Tickets} />
            </Switch>
          </Router>
        </Provider>

      </div>
    );
  }
}

export default App;
