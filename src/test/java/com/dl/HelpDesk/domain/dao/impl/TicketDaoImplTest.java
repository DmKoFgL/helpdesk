package com.dl.HelpDesk.domain.dao.impl;

import com.dl.HelpDesk.config.spring.ApplicationConfig;
import com.dl.HelpDesk.domain.dao.abstracts.TicketDao;
import com.dl.HelpDesk.domain.dao.abstracts.UserDao;
import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.utils.TicketFilter;
import com.dl.HelpDesk.utils.TicketFilterFields;
import com.dl.HelpDesk.utils.TicketSorter;
import com.dl.HelpDesk.utils.TicketSorterField;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class})
@WebAppConfiguration
@Transactional
public class TicketDaoImplTest {
    private Ticket ticket;
    @Autowired
    private TicketDao ticketDao;
    @Autowired
    private UserDao userDao;

    private User user;
    private User manager;
    private User engineer;

    @Before
    public void loadUsers() {
        user = userDao.getAllUsers().get(0);
        manager = userDao.getAllUsers().get(2);
        engineer = userDao.getAllUsers().get(4);

    }

    @Test
    public void testSorting() {
        List<Ticket> usersTickets = ticketDao.getFilteredAllTickets(user, Optional.empty(), TicketSorter.DEFAULT);
        TicketSorter sorter = new TicketSorter(TicketSorterField.ID, true);
        List<Ticket> sortedTickets = ticketDao.getFilteredAllTickets(user, Optional.empty(), sorter);

        Assert.assertNotEquals(usersTickets, sortedTickets);

        usersTickets.sort(Comparator.comparing(Ticket::getId));
        sortedTickets.sort(Comparator.comparing(Ticket::getId));

        Assert.assertEquals(usersTickets, sortedTickets);
    }

    @Test
    public void testFilter() {
        List<Ticket> usersTickets = ticketDao.getFilteredAllTickets(user, Optional.empty(), TicketSorter.DEFAULT);
        TicketFilter filter = new TicketFilter(TicketFilterFields.NAME, "1");
        List<Ticket> filteredTickets = ticketDao.getFilteredAllTickets(user, Optional.of(filter), TicketSorter.DEFAULT);

        Assert.assertNotEquals(usersTickets, filteredTickets);
    }

    @Test
    public void getAllTickets() {
        List<Ticket> usersTickets = ticketDao.getFilteredAllTickets(user, Optional.empty(), TicketSorter.DEFAULT);
        List<Ticket> managersTickets = ticketDao.getFilteredAllTickets(manager, Optional.empty(), TicketSorter.DEFAULT);
        List<Ticket> engineersTickets = ticketDao.getFilteredAllTickets(engineer, Optional.empty(), TicketSorter.DEFAULT);

        Assert.assertTrue(usersTickets.size() != 0);
        Assert.assertTrue(managersTickets.size() != 0);
        Assert.assertTrue(engineersTickets.size() != 0);


    }


}