package com.dl.HelpDesk.domain.dao.impl;

import com.dl.HelpDesk.config.spring.ApplicationConfig;
import com.dl.HelpDesk.config.spring.HibernateConfig;
import com.dl.HelpDesk.domain.dao.abstracts.UserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class,HibernateConfig.class})
@WebAppConfiguration
@Transactional
public class UserDaoImplTest {
    @Autowired
    private UserDao userDao;

    @Test
    public void getAllUsers() {
        Assert.notEmpty( userDao.getAllUsers());

    }
}