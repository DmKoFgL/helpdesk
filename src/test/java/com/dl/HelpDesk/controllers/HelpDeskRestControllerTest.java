package com.dl.HelpDesk.controllers;

import com.dl.HelpDesk.config.spring.ApplicationConfig;
import io.restassured.RestAssured;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class})
@WebAppConfiguration
@Transactional
public class HelpDeskRestControllerTest {
    @Autowired
    private WebApplicationContext webAppContextSetup;

    @Before
    public void setUp() throws Exception {
        RestAssured.port = 8088;
        RestAssuredMockMvc.standaloneSetup(HelpDeskRestController.class);
        RestAssuredMockMvc.webAppContextSetup(webAppContextSetup);

    }

    @WithUserDetails(value = "manager1_mogilev@yopmail.com")
    @Test
    public void getFilteredAndSorteredTicketsPositive() {
        MvcResult res = RestAssuredMockMvc.given()
                .param("field", "id")
                .param("value", "1")
                .param("sorterField", "id")
                .param("asc", "true")
                .when()
                .get("/rest/tickets/MY")
                .mvcResult();
        int statusCode = res.getResponse().getStatus();
        Assert.assertEquals(HttpStatus.OK.value(), statusCode);

    }

    @WithUserDetails(value = "manager1_mogilev@yopmail.com")
    @Test
    public void getSorteredTickets() {
        MvcResult res = RestAssuredMockMvc.given()
                .param("sorterField", "id")
                .param("asc", "true")
                .when()
                .get("/rest/tickets/MY")
                .mvcResult();
        int statusCode = res.getResponse().getStatus();
        Assert.assertEquals(HttpStatus.OK.value(), statusCode);
    }

    @WithUserDetails(value = "manager1_mogilev@yopmail.com")
    @Test
    public void getFilteredTickets() {
        MvcResult res = RestAssuredMockMvc.given()
                .param("field", "id")
                .param("value", "1")
                .when()
                .get("/rest/tickets/MY")
                .mvcResult();
        int statusCode = res.getResponse().getStatus();
        Assert.assertEquals(HttpStatus.OK.value(), statusCode);
    }

    @WithUserDetails(value = "manager1_mogilev@yopmail.com")
    @Test
    public void getTickets() {
        MvcResult res = RestAssuredMockMvc.given()
                .when()
                .get("/rest/tickets/MY")
                .mvcResult();
        int statusCode = res.getResponse().getStatus();
        Assert.assertEquals(HttpStatus.OK.value(), statusCode);
    }
}