package com.dl.HelpDesk.utils.converters;

import com.dl.HelpDesk.utils.TicketFilterFields;
import com.dl.HelpDesk.utils.TicketSorterField;
import com.dl.HelpDesk.utils.TicketsMode;
import org.junit.Assert;
import org.junit.Test;

public class ConvertersTest {

    @Test
    public void ticketFilterFieldsConverterPositive() {
        TicketFilterFieldsConverter converter = new TicketFilterFieldsConverter();

        Assert.assertEquals(TicketFilterFields.ID,converter.convert("id"));
        Assert.assertEquals(TicketFilterFields.ID,converter.convert("Id"));
        Assert.assertEquals(TicketFilterFields.ID,converter.convert("ID"));

        Assert.assertEquals(TicketFilterFields.DESIRED_DATE,converter.convert("DESIRED_DATE"));
        Assert.assertEquals(TicketFilterFields.DESIRED_DATE,converter.convert("Desired_DATE"));
        Assert.assertEquals(TicketFilterFields.DESIRED_DATE,converter.convert("desired_date"));

    }
    @Test(  expected = IllegalArgumentException.class)
    public void ticketFilterFieldsConverterNegative() {
        TicketFilterFieldsConverter converter = new TicketFilterFieldsConverter();
       converter.convert("idd");
    }
    @Test
    public void ticketsModeConverterPositive(){
        TicketsModeConverter converter = new TicketsModeConverter();

        Assert.assertEquals(TicketsMode.ALL,converter.convert("ALL"));
        Assert.assertEquals(TicketsMode.ALL,converter.convert("all"));
        Assert.assertEquals(TicketsMode.MY,converter.convert("My"));
    }
    @Test( expected = IllegalArgumentException.class)
    public void ticketsModeConverterNegative(){
        TicketsModeConverter converter = new TicketsModeConverter();

      converter.convert("Allive");
    }

    @Test
    public void ticketSorterPositive(){
        TicketSorterFieldConverter converter = new TicketSorterFieldConverter();

        Assert.assertEquals(TicketSorterField.DEFAULT,converter.convert("default"));
        Assert.assertEquals(TicketSorterField.ID,converter.convert("ID"));
        Assert.assertEquals(TicketSorterField.NAME,converter.convert("Name"));
    }
    @Test
    public void ticketSorterNegative(){
        TicketSorterFieldConverter converter = new TicketSorterFieldConverter();

        Assert.assertEquals(TicketSorterField.DEFAULT, converter.convert("identity"));
    }
}