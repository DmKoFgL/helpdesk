package com.dl.HelpDesk.utils.converters;

import com.dl.HelpDesk.utils.TicketsMode;
import org.springframework.core.convert.converter.Converter;

public class TicketsModeConverter implements Converter<String, TicketsMode> {
    @Override
    public TicketsMode convert(String source) {
        TicketsMode result = TicketsMode.valueOf(source.toUpperCase());
        return result;
    }
}
