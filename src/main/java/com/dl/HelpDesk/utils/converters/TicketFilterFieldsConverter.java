package com.dl.HelpDesk.utils.converters;

import com.dl.HelpDesk.utils.TicketFilterFields;
import org.springframework.core.convert.converter.Converter;

public class TicketFilterFieldsConverter implements Converter<String, TicketFilterFields> {
    @Override
    public TicketFilterFields convert(String source) {
        TicketFilterFields ticketFilterField;

        ticketFilterField = TicketFilterFields.valueOf(source.toUpperCase());

        return ticketFilterField;
    }
}
