package com.dl.HelpDesk.utils.converters;

import com.dl.HelpDesk.utils.TicketSorterField;
import org.springframework.core.convert.converter.Converter;

public class TicketSorterFieldConverter implements Converter<String,TicketSorterField> {
    @Override
    public TicketSorterField convert(String source) {
        TicketSorterField ticketSorterField;
        try {
             ticketSorterField = TicketSorterField.valueOf(source.toUpperCase());
        }catch (IllegalArgumentException e){
            ticketSorterField = TicketSorterField.DEFAULT;
        }
        return ticketSorterField;
    }
}
