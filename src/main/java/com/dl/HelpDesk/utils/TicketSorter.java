package com.dl.HelpDesk.utils;

public class TicketSorter {
    public static TicketSorter DEFAULT = new TicketSorter(TicketSorterField.DEFAULT, true);

    private TicketSorterField sorterField;
    private Boolean asc = true;

    public TicketSorter(TicketSorterField sorterField, Boolean asc) {
        this.sorterField = sorterField;
        this.asc = asc;
    }

    public TicketSorterField getSorterField() {
        return sorterField;
    }

    public void setAsc(Boolean asc) {
        this.asc = asc;
    }


    public Boolean isAsc() {
        return asc;
    }


}
