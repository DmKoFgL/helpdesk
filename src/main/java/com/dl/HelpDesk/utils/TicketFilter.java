package com.dl.HelpDesk.utils;


public class TicketFilter {
    public static final TicketFilter DEFAULT = new TicketFilter(TicketFilterFields.UNFILTERED, "");
    private final TicketFilterFields field;
    private String value;

    public TicketFilter(TicketFilterFields field, String value) {
        this.field = field == null ? TicketFilterFields.UNFILTERED : field;
        this.value = value;
    }

    public TicketFilterFields getField() {
        return field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}