package com.dl.HelpDesk.domain.service.abstracts;

import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.utils.TicketFilter;
import com.dl.HelpDesk.utils.TicketSorter;
import com.dl.HelpDesk.utils.TicketSorterField;
import com.dl.HelpDesk.utils.TicketsMode;

import java.util.List;
import java.util.Optional;

public interface TicketService {

    List<Ticket> getTickets(TicketsMode mode, User owner, TicketFilter ticketFilter, TicketSorter ticketSorter);

    List<Ticket> getTickets(TicketsMode mode, User owner, TicketSorter ticketSorter);

    List<Ticket> getTickets(TicketsMode mode, User owner);


}
