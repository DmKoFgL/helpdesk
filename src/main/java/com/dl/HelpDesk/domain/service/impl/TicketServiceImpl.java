package com.dl.HelpDesk.domain.service.impl;

import com.dl.HelpDesk.domain.dao.abstracts.TicketDao;
import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.utils.*;
import com.dl.HelpDesk.domain.service.abstracts.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TicketServiceImpl implements TicketService {
    @Autowired
    private TicketDao ticketDao;


    @Override
    public List<Ticket> getTickets(TicketsMode mode, User owner, TicketFilter ticketFilter, TicketSorter ticketSorter) {
        List<Ticket> tickets;
        Optional<TicketFilter> filter = TicketFilterFields.UNFILTERED == ticketFilter.getField() ?
                Optional.empty()
                : Optional.of(ticketFilter);
        switch (mode) {
            case MY: {


                tickets = ticketDao.getFilteredMyTickets(owner, filter, ticketSorter);
            }
            break;
            case ALL: {
                tickets = ticketDao.getFilteredAllTickets(owner,filter, ticketSorter);
            }
            break;
            default: {
                throw new RuntimeException("doesn't know tickets mode: " + mode);
            }
        }
        return tickets;
    }

    @Override
    public List<Ticket> getTickets(TicketsMode mode, User owner, TicketSorter ticketSorter) {
        return getTickets(mode, owner, TicketFilter.DEFAULT, ticketSorter);
    }

    @Override
    public List<Ticket> getTickets(TicketsMode mode, User owner) {
        return getTickets(mode, owner, TicketFilter.DEFAULT, TicketSorter.DEFAULT);
    }


}