package com.dl.HelpDesk.domain.entity;

import com.dl.HelpDesk.domain.entity.enums.State;
import com.dl.HelpDesk.domain.entity.enums.Urgency;
import com.dl.HelpDesk.utils.TicketView;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.validation.constraints.Future;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Ticket implements Serializable {

    @JsonView({TicketView.TicketList.class})
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @JsonView({TicketView.TicketList.class})
    private String name;

    private String description;

    private Date createdOn;

    @JsonView({TicketView.TicketList.class})
    @Future
    private Date desiredResolutionDate;

    @JsonView({TicketView.TicketList.class})
    @Enumerated(EnumType.ORDINAL)
    private State state;

    @JsonView({TicketView.TicketList.class})
    @Enumerated(EnumType.ORDINAL)
    private Urgency urgency;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "category", referencedColumnName = "id")
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "owner", referencedColumnName = "id")
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "approver", referencedColumnName = "id")
    private User approver;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "assignee", referencedColumnName = "id")
    private User assignee;

    public Ticket() {
    }

    public Ticket(long id, String name, String description, Date createdOn,
                  Date desiredResolutionDate, User assignee, User owner,
                  State state, Category category, Urgency urgency, User approver) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.createdOn = createdOn;
        this.desiredResolutionDate = desiredResolutionDate;
        this.assignee = assignee;
        this.owner = owner;
        this.state = state;
        this.category = category;
        this.urgency = urgency;
        this.approver = approver;
    }

    public Ticket(long id, String name, String description,
                  Date desiredResolutionDate, User assignee, User owner,
                  State state, Category category, Urgency urgency, User approver) {
        this(id, name, description, new Date(), desiredResolutionDate, assignee, owner, state, category, urgency, approver);

    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(Date desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    @Transactional
    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    @Transactional
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    @Transactional
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Transactional
    public User getApprover() {
        return approver;
    }

    public void setApprover(User approver) {
        this.approver = approver;
    }

}
