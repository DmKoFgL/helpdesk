package com.dl.HelpDesk.domain.entity;

import com.dl.HelpDesk.config.spring.secure.validation.Password;
import com.dl.HelpDesk.domain.entity.enums.Role;
import com.dl.HelpDesk.utils.TicketView;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

@Entity
@Table(name = "users")
public class User implements Serializable {
    @Column(name = "id",nullable=false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email")
    private String email;
    @Column(name = "role_id")
    @Enumerated(EnumType.ORDINAL)
    private Role role;
    @Column(name = "password")
    private String password;

    public User() {
    }

    public User(String firstName, String lastName, String email, Role role, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.role = role;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Email(message = "Email Address is not a valid format")
    @NotBlank(message = "Email is compulsory")
    @NotNull(message = "Email is compulsory")
    @Size(max = 100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

//    @Size(min = 6, max = 20)
//    @Password
    @NotBlank(message = "Password is compulsory")
    @NotNull(message = "Password is compulsory")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}

