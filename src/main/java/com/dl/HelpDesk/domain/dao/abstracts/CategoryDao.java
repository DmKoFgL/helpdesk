package com.dl.HelpDesk.domain.dao.abstracts;

import com.dl.HelpDesk.domain.entity.Category;
import org.springframework.stereotype.Repository;

public interface CategoryDao  extends CrudDao<Category>{
  //  void add(Category category);
}
