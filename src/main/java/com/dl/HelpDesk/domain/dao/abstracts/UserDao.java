package com.dl.HelpDesk.domain.dao.abstracts;

import com.dl.HelpDesk.domain.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserDao {
    Optional<User> findById(Long id);

    List<User> getAllUsers();

    void addUser(User user);

    User findUserByEmail(String email);

}
