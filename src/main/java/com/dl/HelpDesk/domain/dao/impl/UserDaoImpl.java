package com.dl.HelpDesk.domain.dao.impl;

import com.dl.HelpDesk.domain.dao.abstracts.UserDao;
import com.dl.HelpDesk.domain.entity.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {
    private final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Optional<User> findById(Long id) {
        User query = (User) sessionFactory.getCurrentSession().load(User.class, id);
        logger.info("user by id:" + id + "-" + query);
        return Optional.empty();
    }

    @Override
    public List<User> getAllUsers() {
        Session session = sessionFactory.getCurrentSession();
        List<User> users = session.createQuery("from User").list();
        return users;
    }

    @Override
    public void addUser(User user) {
        Session session = sessionFactory.getCurrentSession();

        session.persist(user);

        logger.info("add user:" + user);
    }

    @Override
    public User findUserByEmail(String email) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.eq("email", email));
        User result = (User) criteria.uniqueResult();
        return result;
    }


}
