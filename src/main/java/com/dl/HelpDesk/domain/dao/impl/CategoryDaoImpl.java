package com.dl.HelpDesk.domain.dao.impl;

import com.dl.HelpDesk.domain.dao.abstracts.CategoryDao;
import com.dl.HelpDesk.domain.dao.abstracts.HibernateCrudDao;
import com.dl.HelpDesk.domain.entity.Category;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class CategoryDaoImpl extends HibernateCrudDao<Category> implements CategoryDao {
    @Autowired
    private SessionFactory sessionFactory;

}
