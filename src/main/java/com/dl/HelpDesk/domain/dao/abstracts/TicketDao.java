package com.dl.HelpDesk.domain.dao.abstracts;

import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.utils.TicketFilter;
import com.dl.HelpDesk.utils.TicketSorter;
import com.dl.HelpDesk.utils.TicketSorterField;

import java.util.List;
import java.util.Optional;

public interface TicketDao extends CrudDao<Ticket> {


    List<Ticket> getFilteredAllTickets(User user, Optional<TicketFilter> filterByField, TicketSorter ticketSorter);

    List<Ticket> getFilteredMyTickets(User user, Optional<TicketFilter> filterByField, TicketSorter ticketSorter);
}
