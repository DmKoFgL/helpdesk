package com.dl.HelpDesk.domain.dao.abstracts;

import java.util.List;
import java.util.Optional;

public interface CrudDao<T> {
    Optional<T> findById(long id);

    Optional<List<T>> findAll();

    Optional<T> add(T entity);

    Optional<T> update(T entity);

    void delete(long id);
}

