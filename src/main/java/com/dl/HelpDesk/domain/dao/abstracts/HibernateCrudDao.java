package com.dl.HelpDesk.domain.dao.abstracts;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional

public abstract class HibernateCrudDao<T> implements CrudDao<T> {

    private Class<T> persistentClass;

    @Autowired
    private LocalSessionFactoryBean sessionFactoryBean;

    public HibernateCrudDao() {
        this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public Optional<T> findById(long id) {
        return Optional.ofNullable(getCurrentSession().get(persistentClass, id));
    }

    @Override
    public Optional<List<T>> findAll() {
        return Optional.ofNullable(getCurrentSession().createQuery("from " + persistentClass.getName()).list());
    }

    @Override
    public Optional<T> add(T entity) {
        if (entity != null) {
            getCurrentSession().save(entity);
            return Optional.of(entity);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<T> update(T entity) {
        if (entity != null) {
            getCurrentSession().merge(entity);
            return Optional.of(entity);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void delete(long id) {
        Optional<T> optionalEntity = findById(id);
        if (optionalEntity.isPresent()) {
            getCurrentSession().delete(optionalEntity.get());
        }
    }

    protected Criteria createEntityCriteria() {
        return getCurrentSession().createCriteria(persistentClass, persistentClass.getName().split("\\.")[4]);
    }

    protected final Session getCurrentSession() {
        return sessionFactoryBean.getObject().getCurrentSession();
    }
}


