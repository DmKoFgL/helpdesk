package com.dl.HelpDesk.domain.dao.impl;

import com.dl.HelpDesk.domain.dao.abstracts.HibernateCrudDao;
import com.dl.HelpDesk.domain.dao.abstracts.TicketDao;
import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.entity.enums.Role;
import com.dl.HelpDesk.domain.entity.enums.State;
import com.dl.HelpDesk.domain.entity.enums.Urgency;
import com.dl.HelpDesk.utils.TicketFilter;
import com.dl.HelpDesk.utils.TicketFilterFields;
import com.dl.HelpDesk.utils.TicketSorter;
import com.dl.HelpDesk.utils.TicketSorterField;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
public class TicketDaoImpl extends HibernateCrudDao<Ticket> implements TicketDao {
    @Autowired
    private SessionFactory sessionFactory;

    private static final Map<TicketSorterField, Function<Boolean, List<Order>>> sorter = new HashMap<>();
    private static final Map<Role, Function<User, Criterion>> roleFilter = new HashMap<>();
    private static final Map<TicketFilterFields, Function<String, Criterion>> ticketFilter = new HashMap<>();
    private static final Map<Role, Function<User, Criterion>> myFilter = new HashMap<>();

    static {
        setSorter();
        setTicketFilter();
    }

    @Override
    public List<Ticket> getFilteredAllTickets(User user, Optional<TicketFilter> filterByField, TicketSorter ticketSorter) {
        Criteria criteria = getTicketCriteria(RoleFilter.CLASSIC_FILTER, user, filterByField, ticketSorter);
        List<Ticket> tickets = criteria.list();
        return tickets;
    }

    @Override
    public List<Ticket> getFilteredMyTickets(User user, Optional<TicketFilter> filterByField, TicketSorter ticketSorter) {
        Criteria criteria = getTicketCriteria(RoleFilter.MY_FILTER, user, filterByField, ticketSorter);
        List<Ticket> tickets = criteria.list();
        return tickets;
    }


    private Criteria getTicketCriteria(RoleFilter roleFilter, User user, Optional<TicketFilter> filterByField, TicketSorter ticketSorter) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Ticket.class);
        criteria.createAlias("owner", "owner");
        criteria.add(roleFilter.getRoleFilter().get(user.getRole()).apply(user));
        sorter.get(ticketSorter.getSorterField()).apply(ticketSorter.isAsc()).forEach(criteria::addOrder);
        filterByField.ifPresent(filter -> criteria.add(ticketFilter.get(filter.getField()).apply(filter.getValue())));
        return criteria;
    }

    private static void setTicketFilter() {
        ticketFilter.put(TicketFilterFields.ID, (value) -> Restrictions.eq("id", Long.valueOf(value)));
        ticketFilter.put(TicketFilterFields.NAME, (value) -> Restrictions.like("name", value, MatchMode.ANYWHERE));
        ticketFilter.put(TicketFilterFields.DESIRED_DATE, (value) -> {
            List<Date> dates = Arrays.stream(value.split(",")).map(date -> {
                try {
                    return new SimpleDateFormat("yyyy-MM-dd").parse(date);
                } catch (ParseException e) {
                    throw new ClassCastException("can't cast date :" + date);
                }
            }).collect(Collectors.toList());
            return Restrictions.between("desiredResolutionDate", dates.get(0), dates.get(1));
        });
        ticketFilter.put(TicketFilterFields.URGENCY, (value) -> Restrictions.like("urgency", Urgency.valueOf(value)));
        ticketFilter.put(TicketFilterFields.STATUS, (value) -> Restrictions.like("state", State.valueOf(value)));
        ticketFilter.put(TicketFilterFields.UNFILTERED, (value) ->{ throw new RuntimeException("unfiltered in criterion");});

    }

    private static void setSorter() {
        sorter.put(TicketSorterField.ID, (isAsc) -> {
            List<Order> orders = new LinkedList<>();
            String columnName = "id";
            orders.add(isAsc ? Order.asc(columnName) : Order.desc(columnName));
            return orders;
        });
        sorter.put(TicketSorterField.NAME, (isAsc) -> {
            List<Order> orders = new LinkedList<>();
            String columnName = "name";
            orders.add(isAsc ? Order.asc(columnName) : Order.desc(columnName));
            return orders;
        });
        sorter.put(TicketSorterField.DESIRED_DATE, (isAsc) -> {
            List<Order> orders = new LinkedList<>();
            String columnName = "desiredResolutionDate";
            orders.add(isAsc ? Order.asc(columnName) : Order.desc(columnName));
            return orders;
        });
        sorter.put(TicketSorterField.URGENCY, (isAsc) -> {
            List<Order> orders = new LinkedList<>();
            String columnName = "urgency";
            orders.add(isAsc ? Order.asc(columnName) : Order.desc(columnName));
            return orders;
        });
        sorter.put(TicketSorterField.STATUS, (isAsc) -> {
            List<Order> orders = new LinkedList<>();
            String columnName = "state";
            orders.add(isAsc ? Order.asc(columnName) : Order.desc(columnName));
            return orders;
        });
        sorter.put(TicketSorterField.DEFAULT, (isAsc) -> {
            List<Order> orders = new LinkedList<>();
            String columnName = "urgency";
            orders.add(isAsc ? Order.asc(columnName) : Order.desc(columnName));
            columnName = "desiredResolutionDate";
            orders.add(isAsc ? Order.asc(columnName) : Order.desc(columnName));
            return orders;
        });

    }

    private enum RoleFilter {
        CLASSIC_FILTER, MY_FILTER;

        private static final Map<Role, Function<User, Criterion>> roleFilter = new HashMap<>();

        static {
            MY_FILTER.roleFilter.put(Role.EMPLOYEE, (user) -> Restrictions.eq("owner", user));
            MY_FILTER.roleFilter.put(Role.MANAGER, (user) -> {
                Criterion criterionOwner = Restrictions.eq("owner", user);
                Criterion criterionInApproved = Restrictions.and(
                        Restrictions.eq("approver", user),
                        Restrictions.in("state", new Object[]{
                                State.APPROVED,
                        }));
                return (Restrictions.or(criterionOwner, criterionInApproved));

            });
            MY_FILTER.roleFilter.put(Role.ENGINEER, (user) -> Restrictions.eq("assignee", user));

            CLASSIC_FILTER.roleFilter.put(Role.EMPLOYEE, (user) -> Restrictions.eq("owner", user));
            CLASSIC_FILTER.roleFilter.put(Role.MANAGER, (user) -> {
                Criterion criterionOwner = Restrictions.eq("owner", user);
                Criterion criterionByAllEmployeesNew = Restrictions.and(
                        Restrictions.eq("owner.role", Role.EMPLOYEE),
                        Restrictions.eq("state", State.NEW));
                Criterion criterionInApproved = Restrictions.and(
                        Restrictions.eq("approver", user),
                        Restrictions.in("state", new Object[]{
                                State.APPROVED,
                                State.DECLINED,
                                State.CANCELED,
                                State.IN_PROGRESS}));
                return Restrictions.or(criterionOwner, criterionByAllEmployeesNew, criterionInApproved);
            });
            CLASSIC_FILTER.roleFilter.put(Role.ENGINEER, (user) -> {
                Criterion criterionByAllEmployeesAndManagersInApproved = Restrictions.and(
                        Restrictions.in("owner.role", new Object[]{Role.EMPLOYEE, Role.MANAGER}),
                        Restrictions.eq("state", State.APPROVED));
                Criterion criterionAsAssignee = Restrictions.and(
                        Restrictions.in("state", new Object[]{State.IN_PROGRESS, State.DONE}),
                        Restrictions.eq("assignee", user));
                return Restrictions.or(criterionByAllEmployeesAndManagersInApproved, criterionAsAssignee);
            });
        }

        public static Map<Role, Function<User, Criterion>> getRoleFilter() {
            return roleFilter;
        }

    }
}
