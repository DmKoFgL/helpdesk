package com.dl.HelpDesk.domain.Enums;

public enum Role {
    EMPLOYEE, MANAGER, ENGINEER
}
