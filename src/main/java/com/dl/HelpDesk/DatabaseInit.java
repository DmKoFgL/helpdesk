package com.dl.HelpDesk;

import com.dl.HelpDesk.domain.dao.abstracts.CategoryDao;
import com.dl.HelpDesk.domain.dao.abstracts.TicketDao;
import com.dl.HelpDesk.domain.entity.Category;
import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.entity.enums.Role;
import com.dl.HelpDesk.domain.entity.enums.State;
import com.dl.HelpDesk.domain.entity.enums.Urgency;
import com.dl.HelpDesk.domain.service.abstracts.UserService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class DatabaseInit implements InitializingBean {
    @Autowired
    private TicketDao ticketDao;
    @Autowired
    private UserService userService;
    @Autowired
    private CategoryDao categoryDao;


    @Override
    public void afterPropertiesSet() {
        userService.addUser(new User("f", "L", "user1_mogilev@yopmail.com",
                Role.EMPLOYEE, "P@1"));
        userService.addUser(new User("f", "L", "user2_mogilev@yopmail.com",
                Role.EMPLOYEE, "P@ssword1"));
        userService.addUser(new User("f", "L", "manager1_mogilev@yopmail.com",
                Role.MANAGER, "P@ssword1"));
        userService.addUser(new User("f", "L", "manager2_mogilev@yopmail.com",
                Role.MANAGER, "P@ssword1"));
        userService.addUser(new User("f", "L", "engineer1_mogilev@yopmail.com",
                Role.ENGINEER, "P@ssword1"));
        userService.addUser(new User("f", "L", "engineer2_mogilev@yopmail.com",
                Role.ENGINEER, "P@ssword1"));

        Date date = new Date(2019, 10, 1);
        User user = userService.getAllUsers().get(0);
        Category category = new Category(1, "testCategory");
        categoryDao.add(category);
        Ticket ticket = new Ticket(1L, "testName1", "descr",
                new Date(3765467897865L), user, user, State.NEW, category, Urgency.LOW, user);
        ticketDao.add(ticket);
        Ticket salt;
        salt = new Ticket(2L, "testName2", "descr",
                new Date(3765567897865L), user, user, State.DRAFT, category, Urgency.AVERAGE, user);
        ticketDao.add(salt);
        user = userService.getAllUsers().get(1);
        salt = new Ticket(3L, "testName3", "descr",
                new Date(3_765_567_557_865L), user, user, State.APPROVED, category, Urgency.CRITICAL, user);
        ticketDao.add(salt);
        salt = new Ticket(4L, "testName4", "descr", new Date(3_765_787_557_865L)
                , user, user, State.APPROVED, category, Urgency.CRITICAL, user);
        ticketDao.add(salt);
        salt = new Ticket(5L, "testName5", "descr", new Date(3_765_787_557_865L)
                , user, user, State.NEW, category, Urgency.CRITICAL, user);
        ticketDao.add(salt);
        salt = new Ticket(6L, "testName6", "descr", new Date(3_765_787_557_865L)
                , user, user, State.APPROVED, category, Urgency.CRITICAL, user);
        ticketDao.add(salt);
    }
}
