package com.dl.HelpDesk.controllers;

import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.service.abstracts.TicketService;
import com.dl.HelpDesk.domain.service.abstracts.UserService;
import com.dl.HelpDesk.utils.*;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping("/rest")
public class HelpDeskRestController {
    @Autowired
    private TicketService ticketService;
    @Autowired
    private UserService userService;
    @JsonView(TicketView.TicketList.class)
    @RequestMapping(value = "/tickets/{mode}", method = RequestMethod.GET, params = {"sorterField", "field"})
    public ResponseEntity<List<Ticket>> getFilteredAndSorteredTickets(@AuthenticationPrincipal UserDetails principal,
                                                                      @PathVariable(value = "mode") TicketsMode ticketsMode,
                                                                      TicketFilter filter, TicketSorter ticketSorter) {
        User currentUser = userService.findUserByEmail(principal.getUsername());
        List<Ticket> tickets = ticketService.getTickets(ticketsMode, currentUser, filter, ticketSorter);
        return new ResponseEntity<>(tickets, HttpStatus.OK);
    }

    @JsonView(TicketView.TicketList.class)
    @RequestMapping(value = "/tickets/{mode}", method = RequestMethod.GET, params = {"field"})
    public ResponseEntity<List<Ticket>> getFilteredTickets(@AuthenticationPrincipal UserDetails principal,
                                                           @PathVariable(value = "mode") TicketsMode ticketsMode,
                                                           TicketFilter filter) {
        User currentUser = userService.findUserByEmail(principal.getUsername());
        List<Ticket> tickets = ticketService.getTickets(ticketsMode, currentUser, filter, TicketSorter.DEFAULT);
        return new ResponseEntity<>(tickets, HttpStatus.OK);
    }

    @JsonView(TicketView.TicketList.class)
    @RequestMapping(value = "/tickets/{mode}", method = RequestMethod.GET, params = {"sorterField"})
    public ResponseEntity<List<Ticket>> getSorteredTickets(@AuthenticationPrincipal UserDetails principal,
                                                           @PathVariable(value = "mode") TicketsMode ticketsMode,
                                                           TicketSorter ticketSorter) {
        User currentUser = userService.findUserByEmail(principal.getUsername());
        List<Ticket> tickets = ticketService.getTickets(ticketsMode, currentUser, ticketSorter);
        return new ResponseEntity<>(tickets, HttpStatus.OK);
    }

    @JsonView(TicketView.TicketList.class)
    @RequestMapping(value = "/tickets/{mode}", method = RequestMethod.GET)
    public ResponseEntity<List<Ticket>> getTickets(@AuthenticationPrincipal UserDetails principal,
                                                   @PathVariable(value = "mode") TicketsMode ticketsMode) {
        User currentUser = userService.findUserByEmail(principal.getUsername());
        List<Ticket> tickets = ticketService.getTickets(ticketsMode, currentUser);
        return new ResponseEntity<>(tickets, HttpStatus.OK);
    }

}
