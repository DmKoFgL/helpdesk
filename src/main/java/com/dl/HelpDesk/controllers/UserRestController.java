package com.dl.HelpDesk.controllers;

import com.dl.HelpDesk.config.spring.secure.UserDto;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.service.abstracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserRestController {
    @Autowired
    private UserService userService ;
    public ResponseEntity<User> getUserInfo(@AuthenticationPrincipal UserDto principal){
        User user =userService.findUserByEmail(principal.getUsername());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
