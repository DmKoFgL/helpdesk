package com.dl.HelpDesk.config.spring.secure;

import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.entity.enums.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserDto extends org.springframework.security.core.userdetails.User implements UserDetails {
    private Role role;
    public UserDto(User user) {
        super(user.getEmail(), user.getPassword(), new ArrayList<>());
        role = user.getRole();
    }

    public UserDto(String username, String password, Collection<? extends GrantedAuthority> authorities, Role role) {
        super(username, password, authorities);
        this.role = role;
    }
}
