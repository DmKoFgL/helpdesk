package com.dl.HelpDesk.config.spring.secure;

import com.dl.HelpDesk.config.spring.secure.UserDto;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.service.abstracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("CustomUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserService userService;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userService.findUserByEmail(login);
        if (user == null) {
            throw new UsernameNotFoundException("Username not found");
        }
        UserDetails userDetails = new UserDto(user);
        return userDetails;
    }
}
